%NotAndAB.m
% Realizando operacao aritmetica NAND nas matrizes A e B

function NotAnd = NotAndAB (A, B)
	NotAnd = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			if A(i,j) != B (i,j) || A(i,j) == 0
				NotAnd(i,j) = 1;
			end
		end
	end 
end