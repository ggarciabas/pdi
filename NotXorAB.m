%NotXorAB.m
% Realizando operacao aritmetica NXOR nas matrizes A e B

function NotXor = NotXorAB (A, B)
	NotXor = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			if A(i,j) == 0
				NotXor(i,j) = !B(i,j);
			else if A(i,j) == 1
				NotXor(i,j) = B(i,j);
			end
		end
	end  
end