%MatrizZeros.m
% Criar matriz XxY e zerar esta

function Zeros = MatrizZeros(X, Y)
	for i=1:X
		for j=1:Y
			Zeros(i,j) = 0;
		end
	end
end