%Ex1Slide4.m

clear all; clc; close all;

A = [
		81 80 198 84 4; 
		87 188 89 89 8; 
		88 95 94 89 89; 
		88 88 97 98 9; 
		82 93 85 96 88
	]

% Media 3x3
N = 3;
Media3x3 = filtromedia (A, N)

% Media 5x5
N = 5;
Media5x5 = filtromedia (A, N);

% Media 3x3 limiar 10
N = 3;
M = 10;
Media3x3L10 = filtromedialimiar (A, N, M);

% Media 5x5 limiar 10
N = 5;
M = 10;
Media5x5L10 = filtromedialimiar (A, N, M);

% Mediana 3x3
N = 3;
Mediana3x3 = filtromediana (A, N);

% Media 3x3 vizinhos 7
N = 3;
V = 7;
Media3x3V7 = filtromediavizinhos (A, N, V);

% Mediana 5x5 separavel
N = 5;
Mediana5x5Sep = filtroMedianaSeparavel(A, N);
