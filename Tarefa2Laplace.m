%Tarefa2Laplace.m
% Parte da tarefa 2 relativo aos testes com Laplace

function void = Tarefa2Laplace (F, M)
	Laplace = FiltroLaplace (F);

	%figure(9);
	%imshow (Laplace, M) 
	imwrite(Laplace,'laplace.png','png')
end
