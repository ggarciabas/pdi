%NotA.m
% Realizando operacao aritmetica NOT na matriz A

function Not = NotA (A)
	Not = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			Not(i,j) = !A(i,j);
		end
	end 
end