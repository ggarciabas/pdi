%filtromedia.m

% Filtro Media 
% A = matriz da imagem
% N = parametro do filtro da media (3, 5, 7, 9, ...)

% EXEMPLO ------

% Matriz A
%              81 80 198 84 4; 
%              87 188 89 89 8; 
%              88 95 94 89 89; 
%              88 88 97 98 9; 
%              82 93 85 96 88

% Matriz A2
%      --- Coluna replicada de acordo com o valor de N (verificar funcao
%     |      replicaX
%     |
%     81    81    80   198    84     4     4 -- Linha replicada
%     81    81    80   198    84     4     4
%     87    87   188    89    89     8     8
%     88    88    95    94    89    89    89
%     88    88    88    97    98     9     9
%     82    82    93    85    96    88    88
%     82    82    93    85    96    88    88

% Matriz A3
%     94   120   121    84    32
%     97   111   112    83    52
%    100   102   103    74    54
%     88    90    93    83    73
%     86    88    92    82    73


function A3 = filtromedia (A, N)
   K = round(N/2)-1;
   A2 = replicamatrizx(A,K);
   for i=K+1:size(A2,1)-K
       for j=K+1:size(A2,2)-K
          A3(i-K,j-K) = sum(sum(A2(i-K:i+K,j-K:j+K))) / (N^2);
       end
   end
   A3 = round(A3);
end
