%NormalizarA.m

function Normalizar = NormalizarA (A)
	min = 10000000;
	max = -1;
	for i=1:size(A,1)
		for j=1:size(A,2)
			if min > A(i,j)
				min = A(i,j);
			else if max < A(i,j)
				max = A(i,j);
			end
		end
	end

	for i=1:size(A,1)
		for j=1:size(A,2)
			A(i,j) = (255 / (max - min)) * (A(i,j) - min);
		end
	end
end