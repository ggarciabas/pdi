%PlusAB.m
% Realizand operacao aritmetica + nas matrizes A e B

function Plus = PlusAB (A, B) 
	Plus = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			valor = A(i,j) + B(i,j);
			if valor < 0
				valor = 0;
			else if valor > 255
				valor = 255;
			end
			Plus(i,j) = valor;
		end
	end 
end