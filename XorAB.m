%XorAB.m
% Realizado operacao aritmetica XOR nas matrizes A e B

function Xor = XorAB (A, B)
	Xor = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			if A(i,j) == 0
				Xor(i,j) = B(i,j);
			else if A(i,j) == 1
				Xor(i,j) = !B(i,j);
			end
		end
	end  
end