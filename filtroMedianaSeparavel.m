%filtroMedianaSeparavel.m
% Filtro da Mediana serparável

function A4 = filtroMedianaSeparavel (A, N)
   K = round(N/2)-1; % numero de valores a ser replicado
   A2 = replicamatrizx(A,K);
   % Avaliando linha
   A3 = [];
   for i=K+1:size(A2,1)-K
       for j=K+1:size(A2,2)-K
    			Atemp = A2(i-K:i+K,j-K:j+K);
    			V = Atemp(K+1,:);
    			V = sort(V);
    			A3(i-K,j-K) = V(round(N / 2));
       end
   end
   A2 = replicamatrizx(A3,K);
   % Avaliando coluna
   for i=K+1:size(A2,1)-K
       for j=K+1:size(A2,2)-K
            Atemp = A2(i-K:i+K,j-K:j+K);
      			V = Atemp(:,K+1);
      			V = sort(V);
      			A4(i-K,j-K) = V(round(N / 2));
       end
   end
   A4 = round(A4);	
end