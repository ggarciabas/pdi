%NotOrAB.m
% Realizando operacao artimetica NOR nas matrizes A e B

function NotOr = NotOrAB (A, B)
	NotOr = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			if A(i,j) == 0
				NotOr(i,j) = !B(i,j);
			end
		end
	end 
end
