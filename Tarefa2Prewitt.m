%Tarefa2Prewitt.m
% Parte da tarefa 2 relativo aos testes com Prewitt

function void = Tarefa2Prewitt (F, M)
	PrewittFH = FiltroPrewittHorizontal (F);
	PrewittFV = FiltroPrewittVertical (F);

	figure(16);
	imshow (PrewittFH, M)

	figure(17);
	imshow (PrewittFV, M)

	% And 
	%figure(18);
	%imshow (bitand(PrewittFH, PrewittFV), M)
	imwrite(bitand(PrewittFH, PrewittFV),'prewitt_and.png','png')
	% Or
	%figure(19);
	%imshow (bitor(PrewittFH, PrewittFV), M)
	imwrite(bitor(PrewittFH, PrewittFV),'prewitt_or.png','png')
	% Plus
	%figure(20);
	%imshow (PlusAB(PrewittFH, PrewittFV), M)
	imwrite(PlusAB(PrewittFH, PrewittFV),'prewitt_plus.png','png')
	% Xor
	%figure(21);
	%imshow (bitxor(PrewittFH, PrewittFV), M) 
	imwrite(bitxor(PrewittFH, PrewittFV),'prewitt_xor.png','png')
end