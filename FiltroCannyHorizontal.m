%FiltroCannyHorizontal.m

function CannyH = FiltroCannyHorizontal (A)
	hh = [ 
           -1   -1  -1;
            2    2   2;
           -1   -1  -1
		 ];

	K = round(size(hh,1)/2)-1;
	CannyH = uint8(MatrizZeros(size(A,1), size(A,2)));
	A = replicamatrizx(A, K);

	for i=1+K:size(A,1)-K
      for j=1+K:size(A,2)-K
      	valor = abs(
     					sum(
     							sum(double(A(i-K:i+K,j-K:j+K)).* hh)
     						)
     				);
        if valor < 0
          valor = 0;
        else if valor > 255
          valor = 255;
        end
      	CannyH(i-K,j-K) = valor;
      end
	end
end