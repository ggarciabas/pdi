%Tarefa4Ex7.m

function Tarefa4Ex7 (A, mapA, B, mapB)
    % ----------- ex 7
    figure(1)
    imhist(A)
    figure(2)
    imhist(B)
    
    
    % Media 3x3 
    Media3x3A = filtromedia (A, 3);
    figure(3)
    imhist(Media3x3A)
    figure(4)
    imshow(Media3x3A, mapA)
    
    Media3x3B = filtromedia (B, 3);
    figure(5)
    imhist(Media3x3B)
    figure(6)
    imshow(Media3x3B, mapB)
         
end