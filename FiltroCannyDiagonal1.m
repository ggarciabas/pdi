%FiltroCannyDiagonal1.m

function CannyD1 = FiltroCannyDiagonal1 (A)
	hd1 = [	
			     -1   -1   2;
	         -1    2  -1;
	          2   -1  -1
		 ];

	K = round(size(hd1,1)/2)-1;
	CannyD1 = uint8(MatrizZeros(size(A,1), size(A,2)));
	A = replicamatrizx(A, K);

	for i=1+K:size(A,1)-K
      for j=1+K:size(A,2)-K
      	valor = abs(
     					sum(
     							sum(double(A(i-K:i+K,j-K:j+K)).* hd1)
     						)
     				);
        if valor < 0
          valor = 0;
        else if valor > 255
          valor = 255;
        end
      	CannyD1(i-K,j-K) = valor;
      end
	end
end