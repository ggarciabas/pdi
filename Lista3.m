%Lista3.m

clear all; clc; close all;

[F, M] = imread ('ilha.bmp');

H = MatrizZeros(size(M,1),1); % criando o histograma
FEq = zeros(size(F)); % criando a figura equalizada

for i = 1:size(F,1)
    for j = 1:size(F,2)
        H(F(i,j)+1) = H(F(i,j)+1) + 1; % identificando o total
        % de pixels com determinado nivel de cinza.
        % 0 -> 1
        % 1 -> 2
        % 255 -> 256
    end
end

% Realizando o calculo de Probabilidade P(n_k)
PN_k = H/sum(H); % vetor com os valores

S(1) = PN_k(1);
for i = 2:length(H) % o primeiro nao tem anterior para somar
    S(i) = PN_k(i) + S(i-1);  % funcao de distribuicao acumulada  
end  
S_k = round(S*256); % mutiplica pelo valor de tons 

for i = 1:size(F,1)
    for j = 1:size(F,2)
        FEq(i,j) = S_k(F(i,j) + 1);
    end
end

figure(1)
bar(H, 0.1)

figure(2)
imhist(F)

figure(3)
FEq = uint8(FEq);
imshow(FEq)

figure(4)
histeq(F)

