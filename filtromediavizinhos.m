%filtromediavizinhos.m

function A3 = filtromediavizinhos (A, N, V)
	K = round(N/2)-1;
	vizinhos = V;
	A2 = replicamatrizx(A,K);
	for i=K+1:size(A2,1)-K
	   for j=K+1:size(A2,2)-K
		   Atemp = A2(i-K:i+K,j-K:j+K);
		   V = [];
		   for  v=1:N
			   V = [V Atemp(v,:)];
		   end
		   V = sort(V-A(i-K,j-K));
		   A3(i-K,j-K) = mean(V(1:vizinhos)+A(i-K,j-K));
	   end
	end
	A3 = round(A3);
end
