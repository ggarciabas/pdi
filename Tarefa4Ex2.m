%Tarefa4Ex2.m

function Tarefa4Ex2 (A, map)
    % ----------- ex 2
    N = 3; % nxn
    L = 15; % limiar
    V = 7; % vizinhos

    % Mediana NxN
    MedianaNxN = filtromediana (A, N);
    % figure(2);
    % imshow(MedianaNxN, map)
    imwrite(MedianaNxN, map, 'Lista4Parte1/ex2/mediana_nxn.png', 'png')

    % Media NxN
    MediaNxN = filtromedia (A, N);
    % figure(3);
    % imshow(MediaNxN, map)
    imwrite(MediaNxN, map, 'Lista4Parte1/ex2/media_nxn.png', 'png')

    % Media NxN limiar L
    MediaNxNL = filtromedialimiar (A, N, L);
    % figure(4);
    % imshow(MediaNxNL, map)
    imwrite(MediaNxNL, map, 'Lista4Parte1/ex2/media_nxn_l.png', 'png')

    % Media NxN vizinhos V
    MediaNxNV = filtromediavizinhos (A, N, V);
    % figure(5);
    % imshow(MediaNxNV, map)
    imwrite(MediaNxNV, map, 'Lista4Parte1/ex2/media_nxn_v.png', 'png')

    % Mediana NxN separavel
    MedianaNxNSep = filtroMedianaSeparavel(A, N);
    % figure(6);
    % imshow(MedianaNxNSep, map)
    imwrite(MedianaNxNSep, map, 'Lista4Parte1/ex2/mediana_nxn_sep.png', 'png')
end