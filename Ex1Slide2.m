%Ex1Slide2.m

close all; clear all; clc;

A = [
		0 1 1;
		0 1 0;
		0 0 1
	]

B = [
		1 0 0;
		1 1 0;
		0 1 0
	]

outAnd = AndAB(A, B);
outAnd

outOr = OrAB (A, B);
outOr

outXor = XorAB (A, B);
outXor

outNotA = NotA (A);
outNotA

outNotB = NotA (B);
outNotB

outNotAnd = NotAndAB (A, B);
outNotAnd

outNotOr = NotOrAB (A, B);
outNotOr

outNotXor = NotXorAB (A, B);
outNotXor