%Tarefa4Ex4.m

function Tarefa4Ex4 (A, map)
    % ----------- ex 4
    % Media 3x3 duas vezes
    Media3x3 = filtromedia (A, 3);
    Media3x3 = filtromedia (Media3x3, 3);
    % figure(2);
    % imshow(Media3x3, map)
    imwrite(Media3x3, map, 'Lista4Parte1/ex4/media_3x3_duas.png', 'png')

    % Media 5x5
    Media5x5 = filtromedia (A, 3);
    Media5x5 = filtromedia (Media5x5, 3);
    % figure(2);
    % imshow(Media5x5, map)
    imwrite(Media5x5, map, 'Lista4Parte1/ex4/media_5x5.png', 'png')
end