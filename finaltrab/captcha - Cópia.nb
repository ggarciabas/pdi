(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     29200,        693]
NotebookOptionsPosition[     27926,        650]
NotebookOutlinePosition[     28272,        665]
CellTagsIndexPosition[     28229,        662]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[""], "Input",
 CellChangeTimes->{3.5159576989365*^9}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"image", " ", "=", " ", 
  RowBox[{
   RowBox[{"ColorSeparate", "[", 
    GraphicsBox[
     TagBox[RasterBox[CompressedData["
1:eJztml9unDAQxlHbh94C9Qq9AGqPkB7ASqT0tVJaqcqTj+Kj+LW38FHcgQmj
Wf6tAbMs5PvJWRFY2AW+mfmY9ZenXw8/PxRF8fszvTw8/v328vL4+uMT/fP9
9c/z00da+Ep//+ivXo4AAAAAAAAAAAAAAAAAtiaEegCQBe+jMbEsY1HUgxZo
DQCjBBJNM0ZwrlYRDdIVDdYVDySud0qIsYrRtKNqRtmM4nKUvV1DtPYtQdGC
Xi+6IpmdCcrAVbX3lzgEoZVNFaOl1NMO3wyjdOXUTqGblzolj2TG6+/oLoQ2
dsLCA9A5diIoBz56yvIuurD4i90jvtGMHdoUVNYyl1tCXfsm8pJsuqN8Zdtz
sUv2pvPlM6KF3JCoyEbQ0en1LBpzzaUexKhkFYbfInmpY6U4tPt5bE/cQOJN
hwKEbeQ2kUJykuxf1Vm+2uJTbogfiV8/XAE76JSlCwRdGH4kZCe/QYwvwk09
faTAfnIDOF/RMJ3ScDZK5bsmEaPFF5ydrbZesvWIz4b9oNggCVPVkyvlVyr/
rjHXK6AgJY/Fw56KVpKKuKOlt+5DaNKUazKzmZGyOEA6cAbOChkq0dXxndUY
XonKJu2hE1Q/KWkPtk9B9E3WlZPy1/egs5CTogWdoDiOsiLm6vi2aoIytQIK
OmUN1ghpwu/5eFim6opEJV9YhlYXbc0aIGKu7LJn1QNgZ1RAjdyIwYaVVMMc
uqJK4et77GfuV86o7NxFka6vbqfw+nx9uXdgrsLsCihIe2ewFIquVoe5tiIU
5m5G06BcEi8xDqiLS2Smx5B3YK6q0Z9sUpgodrwpR4zzXSBFUcngdmJy7Sjb
s1v2wa6rrkyN97ObK3vN2YYrzmSsR8rr8/UZqFhIXHMRSctaxSpdMVpdmR5v
T22ugrrsZuQ99oqT7/dI5WfELe06f2TSG9frihF1rW5knd1cVQkVsByXXIvu
VrEJYaObj44DkSJy7aaEVSV+kBy/6ZzaXLmE3o5JMvO64UApK3eDncTDPqRp
TRqpIAk3JeTXVY7fdM5rrsLlVCvbNqWNmo6V9pDIvUS61LlnkmhYWp2R4K9c
fl1x3V8XO9Pm6sgTG1w7i69sVWSaYdvh1HSsMHkkl7dbOKYWjnG6EcldrFKF
Bvd7U/ZKOfDCBimpxdbhZydCgyPojKZrT9h4DFYHvh1zCkdQU6l9hlkNse2d
rtAVnaAu5dyLk+w0cfpgDfKg1N8k9+L23+oNbSNXe8i+xubUdzAbrnedlRTC
EuB7fKkG+XEht5PsaCzvwQFDF5nn5XJ1IKchoto/lr3feirsYR37AaBrq7Uk
4+yTKsEtoNQk6qIFuA4AAAAAAAAAAAAAAAAAAAAAAAAAANDnP2EfgQI=
       "], {{0, 50}, {200, 0}}, {0, 255},
       ColorFunction->RGBColor],
      BoxForm`ImageTag["Byte", ColorSpace -> "RGB", Interleaving -> True],
      Selectable->False],
     BaseStyle->"ImageGraphics",
     ImageSize->{85., Automatic},
     ImageSizeRaw->{200, 50},
     PlotRange->{{0, 200}, {0, 50}}], "]"}], "[", 
   RowBox[{"[", "3", "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"dims", " ", "=", " ", 
   RowBox[{"ImageDimensions", "[", "image", "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"components", " ", "=", " ", 
   RowBox[{"ImageForestingComponents", "[", 
    RowBox[{"image", ",", " ", "Automatic", ",", " ", 
     RowBox[{"{", 
      RowBox[{"1", ",", " ", "20"}], "}"}]}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"cm", " ", "=", " ", 
   RowBox[{"ComponentMeasurements", "[", 
    RowBox[{"components", ",", " ", "\"\<BoundingBox\>\""}], "]"}]}], 
  ";"}], "\n", 
 RowBox[{
  RowBox[{"ct", " ", "=", " ", 
   RowBox[{"Sort", "[", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"i", " ", "/.", " ", "cm"}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"i", ",", " ", "2", ",", " ", 
        RowBox[{"Length", "[", "cm", "]"}]}], "}"}]}], "]"}], "]"}]}], 
  ";"}], "\n", 
 RowBox[{"bin", " ", "=", 
  RowBox[{"Map", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"ImageTake", "[", 
      RowBox[{"image", ",", " ", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"dims", "[", 
           RowBox[{"[", "2", "]"}], "]"}], " ", "-", " ", 
          RowBox[{
           RowBox[{"#", "[", 
            RowBox[{"[", "2", "]"}], "]"}], "[", 
           RowBox[{"[", "2", "]"}], "]"}]}], ",", " ", 
         RowBox[{
          RowBox[{"dims", "[", 
           RowBox[{"[", "2", "]"}], "]"}], " ", "-", " ", 
          RowBox[{
           RowBox[{"#", "[", 
            RowBox[{"[", "1", "]"}], "]"}], "[", 
           RowBox[{"[", "2", "]"}], "]"}]}]}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"#", "[", 
           RowBox[{"[", "1", "]"}], "]"}], "[", 
          RowBox[{"[", "1", "]"}], "]"}], ",", " ", 
         RowBox[{
          RowBox[{"#", "[", 
           RowBox[{"[", "2", "]"}], "]"}], "[", 
          RowBox[{"[", "1", "]"}], "]"}]}], "}"}]}], "]"}], " ", "&"}], ",", 
    " ", "ct"}], "]"}]}]}], "Input",
 CellChangeTimes->CompressedData["
1:eJxTTMoPSmViYGAQBWIQfb48wVjD6JXj+fIeKzD9PcoBQis5Q8TvuUP4NoEQ
vlYohP8rAsJXToLQGVD6y0qIvOcaCN9dSxNMX9AB099XWkHo63YQcTlXCC3s
BhHX8IXwt/hD6CXBEPGTuRD+niII/20FhD5SAxG3rIXQCX0QmmsqRH7qYgid
sxRCvzkIoc8chqhTPQGhlS5BxH1uQOj/dyDic/9CaNb/UHcc0wLT6afAdLn+
OQj/IJgGABk5qxo=
  "]],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJztWF2K3DAMNqUPvYXoFXoB0x5hewCxC9vXwrZQ9klH0VH02lvoKFv/JBMn
sb3xxBAo/mAmM3Ik69dS8vnp58OPD8aYX5/c18Pjn68vL4+v3z+6P99efz8/
+aUv7vPXfd4GBgYGBgYG3oHq1Rp0gCAYA3K1GvdAReafDIDoBhij16nTBLWI
aC2AV9pApBEY8ldHwR6biO0hpQqnqyVmFvHuZ08JcRC/SG6xTRqi7qkSvFKA
IGdYmnHbQ2FyvvItDtoaD2c47Ygc/VMCg4HztvA8w2NSDDT9likuDdIyKiMD
1pj8ztY27bOH0HQ1iQo+IJ5unadwr1gFLDkqQY0HOlXhLMwu/9CXvFgTi19P
yI1uqIVVTWvUa9utj1gfHTAoGqofDgpRYcK1SjamLWKZizue7mI2BWpvwsnU
yzSVYjeuVUewEuSXubD1UKwAtrJk0aglfWFlh8am5C0BrvHQUfnvgPahXSzD
oh1J/1+4EjnCvqP6Q1zJlvbuWB66zaq3NGuxlFf+lo2fYZ/q0RJbLIGO5WFz
pXxLJyilLxsgBtowZe4LltB+IaBfedASWZUVVcO1eO6K+lhyQim8GvOWQEFI
t/JIp8EkjWOyuWELMzwJVpoXX/E5S6S4e36lFWlWpQNEGBdMbu6bVZjuWtTQ
Sq8pzCbdyoMTh2Aa4tBTKluIcSM/rNSo2VGYTXqVRzjjicJDyKoa1a5LeA+J
YwsvFK7YoXm3L+VxbuR1D35gnWOJyD+ELLKYyzzT1YVv1UFg7npZZFqh0jIt
SL8h6yB4zgTapIS6xij5idf1xJwdHF4C+Ehwv+HkKHQ+lLLdIg8plnO05fgQ
1xEY1c+2zwK42iWCLSeVugMK7jQOwy0f5tmNYjuhJxS6Fzo9YeEFe/cF+ycL
vlqLgYGBgYGBgYGBgf8a/wDPswOb
    "], {{0, 50}, {200, 0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag[
   "Byte", ColorSpace -> "Grayscale", ImageSize -> {85., Automatic}, 
    Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSize->{85., Automatic},
  ImageSizeRaw->{200, 50},
  PlotRange->{{0, 200}, {0, 50}}]], "Output",
 CellChangeTimes->{
  3.51596229709275*^9, {3.515962956139625*^9, 3.515962965139625*^9}, 
   3.515963001452125*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJx9kMsNwjAMhi3EgStiAItJLBihDGC1ElyRChLqyaNkFI+SUcqfNIGSA5Zs
y48vsX0c7t1tQ0SPHUzXv07j2E+XLYLz9LwOqXSA7qHzWqI7jKiqCDMlYSSI
xEII7opEmGcnK/3oUfhQn0kNEd5Lgy/AVwDIOq5AlQTYP8AaILaA5Jl/Aa/r
FUBrTRqANS3zATCe5SXJLB8lfxaYWXAkM0tHiW+3xOM5
      "], {{0, 19}, {17, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{17, 19},
    PlotRange->{{0, 17}, {0, 19}}], ",", 
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJxdT8sNwjAMtRAH7nC3mMSCEcoAVivBFakgoZ4ySkfxKB4lxK6TUizl9/ze
88t5eHaPHQC8DmXr+s9lHPvpti+P6/S+D9Y6lXUsK9dSzZsSRgCUFZgRmYsC
gqgJIdlZIHbEu65IALSIWlcry7tuIsEOEzMjKEPmXO0wC4EX+lSxG7Co2Cx0
GrVI5jtXWjhjHYs1kvku0NyUHMINn/7ipgjR4iq3L8XvCVhXxHOkXyArYWqP
LyAwAU0=
      "], {{0, 20}, {19, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{19, 20},
    PlotRange->{{0, 19}, {0, 20}}], ",", 
   GraphicsBox[
    TagBox[
     RasterBox[{{255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
      255, 255, 255, 255}, {255, 255, 255, 255, 255, 255, 255, 255, 255, 223, 
      191, 191, 255, 255, 255, 255}, {255, 255, 255, 255, 255, 255, 255, 255, 
      159, 0, 0, 0, 31, 159, 255, 255}, {255, 255, 255, 255, 255, 255, 255, 
      255, 159, 0, 31, 127, 159, 31, 127, 255}, {255, 255, 255, 255, 255, 255,
       255, 255, 255, 191, 223, 255, 255, 223, 0, 159}, {255, 255, 255, 255, 
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 0}, {255, 255, 255,
       255, 255, 255, 255, 255, 223, 255, 255, 255, 255, 95, 0, 0}, {255, 255,
       255, 255, 255, 255, 255, 191, 0, 63, 95, 95, 31, 0, 0, 0}, {255, 255, 
      255, 255, 255, 255, 255, 255, 191, 0, 0, 0, 0, 0, 0, 159}, {255, 159, 
      255, 255, 255, 255, 255, 255, 255, 95, 0, 127, 191, 191, 255, 255}, {
      159, 0, 63, 255, 255, 255, 255, 255, 255, 127, 0, 63, 255, 255, 255, 
      255}, {223, 0, 0, 255, 255, 255, 255, 255, 255, 31, 0, 0, 255, 255, 255,
       255}, {255, 95, 0, 255, 255, 255, 255, 255, 63, 0, 0, 31, 255, 255, 
      255, 255}, {255, 223, 31, 95, 223, 255, 191, 63, 0, 0, 0, 159, 255, 255,
       255, 255}, {255, 255, 223, 63, 0, 0, 0, 0, 0, 0, 95, 255, 255, 255, 
      255, 255}, {255, 255, 255, 255, 159, 63, 0, 0, 63, 159, 255, 255, 255, 
      255, 255, 255}}, {{0, 16}, {16, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{16, 16},
    PlotRange->{{0, 16}, {0, 16}}], ",", 
   GraphicsBox[
    TagBox[
     RasterBox[{{255, 255, 255, 223, 95, 95, 223}, {255, 255, 255, 127, 0, 0, 
      127}, {255, 255, 255, 159, 0, 0, 159}, {255, 255, 255, 255, 159, 191, 
      255}, {255, 255, 255, 255, 255, 255, 255}, {255, 223, 191, 159, 127, 95,
       191}, {255, 191, 63, 0, 0, 0, 191}, {255, 255, 255, 31, 0, 0, 191}, {
      255, 255, 255, 31, 0, 0, 223}, {255, 255, 255, 31, 0, 0, 223}, {255, 
      255, 255, 31, 0, 0, 255}, {255, 255, 255, 0, 0, 0, 255}, {255, 255, 255,
       0, 0, 0, 255}, {255, 255, 223, 0, 0, 31, 255}, {255, 255, 223, 0, 0, 
      31, 255}, {255, 255, 159, 0, 0, 31, 255}, {255, 31, 0, 0, 0, 0, 63}}, {{
      0, 17}, {7, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{7, 17},
    PlotRange->{{0, 7}, {0, 17}}], ",", 
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJx9jkEKAjEMRT/iwqU7l8WTFD3CeIAyA+NWGAWZVY+So+QoPUpNWtFkoYEE
3k8++cfpNlw3AO47GcP4PC3LuF62Auf1MU+6Okjvpeuv4ugR2SIBZDlRSN6f
g1nqBL8x9qepG0oEIrcPDQO0VAlqYyq5SankT6KuRJRvnqbYiKqEWr3CTqh/
Mvds5kM3kCUmxywJ7D35QHIg718OqjDJ
      "], {{0, 23}, {16, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{16, 23},
    PlotRange->{{0, 16}, {0, 23}}], ",", 
   GraphicsBox[
    TagBox[
     RasterBox[{{255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
      255, 255}, {255, 255, 255, 255, 191, 95, 159, 223, 255, 255, 255, 255, 
      255, 255}, {255, 255, 255, 255, 255, 159, 31, 0, 31, 95, 159, 223, 255, 
      255}, {255, 255, 255, 255, 255, 255, 95, 0, 0, 0, 63, 63, 255, 255}, {
      255, 255, 255, 255, 255, 255, 31, 0, 0, 95, 255, 255, 255, 255}, {255, 
      255, 255, 255, 255, 223, 0, 0, 0, 191, 255, 255, 255, 255}, {255, 255, 
      255, 255, 255, 159, 0, 0, 0, 223, 255, 255, 255, 255}, {255, 255, 255, 
      255, 255, 95, 0, 0, 63, 255, 255, 255, 255, 255}, {255, 255, 255, 255, 
      255, 31, 0, 0, 127, 255, 255, 255, 255, 255}, {255, 255, 255, 255, 223, 
      0, 0, 0, 191, 255, 255, 255, 255, 255}, {255, 255, 255, 255, 159, 0, 0, 
      0, 223, 255, 255, 255, 255, 255}, {255, 255, 255, 255, 95, 0, 0, 63, 
      255, 255, 255, 255, 255, 255}, {255, 255, 255, 255, 31, 0, 0, 127, 255, 
      255, 255, 255, 255, 255}, {255, 255, 255, 223, 0, 0, 0, 191, 255, 255, 
      255, 255, 255, 255}, {255, 255, 255, 159, 0, 0, 0, 223, 255, 255, 255, 
      255, 255, 255}, {255, 255, 255, 95, 0, 0, 63, 255, 255, 255, 255, 255, 
      255, 255}, {255, 255, 255, 31, 0, 0, 127, 255, 255, 255, 255, 255, 255, 
      159}, {223, 127, 127, 0, 0, 0, 159, 255, 255, 255, 255, 255, 191, 0}, {
      223, 159, 95, 31, 0, 0, 31, 159, 223, 255, 255, 159, 0, 63}, {255, 255, 
      255, 255, 223, 159, 95, 31, 0, 0, 0, 0, 0, 159}, {255, 255, 255, 255, 
      255, 255, 255, 255, 223, 159, 95, 31, 0, 255}}, {{0, 21}, {14, 0}}, {0, 
      255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{14, 21},
    PlotRange->{{0, 14}, {0, 21}}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.51596229709275*^9, {3.515962956139625*^9, 3.515962965139625*^9}, 
   3.515963001624*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"l1", " ", "=", " ", 
    RowBox[{"bin", "[", 
     RowBox[{"[", "1", "]"}], "]"}]}], "\[IndentingNewLine]", 
   RowBox[{"l2", " ", "=", " ", 
    RowBox[{"ImageRotate", "[", 
     RowBox[{
      RowBox[{"bin", "[", 
       RowBox[{"[", "2", "]"}], "]"}], ",", " ", 
      RowBox[{
       RowBox[{"-", "25"}], " ", "Degree"}], ",", " ", 
      RowBox[{"Background", " ", "->", " ", "White"}]}], "]"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"l3", " ", "=", " ", 
    RowBox[{"ImageRotate", "[", 
     RowBox[{
      RowBox[{"bin", "[", 
       RowBox[{"[", "3", "]"}], "]"}], ",", " ", 
      RowBox[{"40", " ", "Degree"}], ",", " ", 
      RowBox[{"Background", " ", "->", " ", "White"}]}], "]"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"l4", " ", "=", " ", 
    RowBox[{"bin", "[", 
     RowBox[{"[", "4", "]"}], "]"}]}], "\[IndentingNewLine]", 
   RowBox[{"l5", " ", "=", " ", 
    RowBox[{"ImageRotate", "[", 
     RowBox[{
      RowBox[{"bin", "[", 
       RowBox[{"[", "5", "]"}], "]"}], ",", " ", 
      RowBox[{
       RowBox[{"-", "40"}], " ", "Degree"}], ",", " ", 
      RowBox[{"Background", " ", "->", " ", "White"}]}], "]"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"l6", " ", "=", " ", 
    RowBox[{"ImageRotate", "[", 
     RowBox[{
      RowBox[{"bin", "[", 
       RowBox[{"[", "6", "]"}], "]"}], ",", " ", 
      RowBox[{"10", " ", "Degree"}], ",", " ", 
      RowBox[{"Background", " ", "->", " ", "White"}]}], "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.515958041389625*^9, 3.5159582059365*^9}, {
  3.5159583464365*^9, 3.515958354014625*^9}, {3.5159607596865*^9, 
  3.515960761202125*^9}, {3.515961440624*^9, 3.515961469827125*^9}, {
  3.51596151784275*^9, 3.515961545670875*^9}, {3.515961583639625*^9, 
  3.515961588639625*^9}, {3.5159623188115*^9, 3.515962451983375*^9}, {
  3.5159625045615*^9, 3.5159625941865*^9}, {3.515962626202125*^9, 
  3.515962636795875*^9}, {3.515962674999*^9, 3.515962685374*^9}}],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJx9kMsNwjAMhi3EgStiAItJLBihDGC1ElyRChLqyaNkFI+SUcqfNIGSA5Zs
y48vsX0c7t1tQ0SPHUzXv07j2E+XLYLz9LwOqXSA7qHzWqI7jKiqCDMlYSSI
xEII7opEmGcnK/3oUfhQn0kNEd5Lgy/AVwDIOq5AlQTYP8AaILaA5Jl/Aa/r
FUBrTRqANS3zATCe5SXJLB8lfxaYWXAkM0tHiW+3xOM5
    "], {{0, 19}, {17, 0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSizeRaw->{17, 19},
  PlotRange->{{0, 17}, {0, 19}}]], "Output",
 CellChangeTimes->{{3.515962326764625*^9, 3.515962351170875*^9}, {
   3.515962387327125*^9, 3.515962406483375*^9}, 3.515962454795875*^9, {
   3.51596250659275*^9, 3.51596259628025*^9}, {3.515962632999*^9, 
   3.515962638608375*^9}, 3.51596268703025*^9, 3.515962971733375*^9, 
   3.515963005577125*^9}],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJy9UcENwjAMtBAPZoCHxSQRjFAGsFoJvkgFCfXlUTJKR/EowU4TSNJ/LblJ
enf2OTkPz+6xA4DXQT9d/7mMYz/d9nq4Tu/7YNBJ86gZNg0KkrciJeCJgNEj
EkqNBC9qU0FlrNwqMC8rzhVgGkorrDSxAS+Mf8zagJPGtRqMjFYSgrPWDrKR
opr9A7JsNRjJYn18BfhcxmTYapYNNwhzmsco1ajyO1PbKN+BIZU7dc2civkS
EZvR6Ty8uhyzwEBSPO5W8QUi5z2v
    "], {{0, 27}, {26, 0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSizeRaw->{26, 27},
  PlotRange->{{0, 26}, {0, 27}}]], "Output",
 CellChangeTimes->{{3.515962326764625*^9, 3.515962351170875*^9}, {
   3.515962387327125*^9, 3.515962406483375*^9}, 3.515962454795875*^9, {
   3.51596250659275*^9, 3.51596259628025*^9}, {3.515962632999*^9, 
   3.515962638608375*^9}, 3.51596268703025*^9, 3.515962971733375*^9, 
   3.51596300559275*^9}],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJytj7ENAjEMRb8QBRtQRkwSwQjHANadBC3SgYSuyigexaN4lOAcNMQuKLAU
R3l2vr8P0224bgDcd5aG8Xmc53E5b+1xWh6XqZX2n1P/HVw8E6SkbpaR0lLH
GckyAdxhUbt6vAalaBEiiDiK3MY6h1XIKl6k1oLVkBcCB1ZMneLuL/H3T8nd
Oty61DUbBmnvj9UW18w5GKeRh5/iBZXm0lc=
    "], {{0, 23}, {23, 0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSizeRaw->{23, 23},
  PlotRange->{{0, 23}, {0, 23}}]], "Output",
 CellChangeTimes->{{3.515962326764625*^9, 3.515962351170875*^9}, {
   3.515962387327125*^9, 3.515962406483375*^9}, 3.515962454795875*^9, {
   3.51596250659275*^9, 3.51596259628025*^9}, {3.515962632999*^9, 
   3.515962638608375*^9}, 3.51596268703025*^9, 3.515962971733375*^9, 
   3.515963005608375*^9}],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[{{255, 255, 255, 223, 95, 95, 223}, {255, 255, 255, 127, 0,
     0, 127}, {255, 255, 255, 159, 0, 0, 159}, {255, 255, 255, 255, 159, 191, 
    255}, {255, 255, 255, 255, 255, 255, 255}, {255, 223, 191, 159, 127, 95, 
    191}, {255, 191, 63, 0, 0, 0, 191}, {255, 255, 255, 31, 0, 0, 191}, {255, 
    255, 255, 31, 0, 0, 223}, {255, 255, 255, 31, 0, 0, 223}, {255, 255, 255, 
    31, 0, 0, 255}, {255, 255, 255, 0, 0, 0, 255}, {255, 255, 255, 0, 0, 0, 
    255}, {255, 255, 223, 0, 0, 31, 255}, {255, 255, 223, 0, 0, 31, 255}, {
    255, 255, 159, 0, 0, 31, 255}, {255, 31, 0, 0, 0, 0, 63}}, {{0, 17}, {7, 
    0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSizeRaw->{7, 17},
  PlotRange->{{0, 7}, {0, 17}}]], "Output",
 CellChangeTimes->{{3.515962326764625*^9, 3.515962351170875*^9}, {
   3.515962387327125*^9, 3.515962406483375*^9}, 3.515962454795875*^9, {
   3.51596250659275*^9, 3.51596259628025*^9}, {3.515962632999*^9, 
   3.515962638608375*^9}, 3.51596268703025*^9, 3.515962971733375*^9, 
   3.515963005608375*^9}],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJzdUcEJAjEQHMSHRQjBSoKWcBYQ7uD8Cqcg90opW8qWsqWsSU4/d7sW4EBC
wjCT2clpuHe3HYDHoWxd/zpPUz9f9+VymZ/jUKnjZ+n/gEVEYzsSg1dkQIwg
1URAXimlFNGEFGhjm4uymuVkvBmWBs0aM6owJbbIJmTLsqQAkgaTasIctym/
whgdnZYZxeMIIXucwotS07BLmX0tEMeyfBuFIE4MwPidBuYfs7Ebw8Ib48TP
aA==
    "], {{0, 28}, {28, 0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSizeRaw->{28, 28},
  PlotRange->{{0, 28}, {0, 28}}]], "Output",
 CellChangeTimes->{{3.515962326764625*^9, 3.515962351170875*^9}, {
   3.515962387327125*^9, 3.515962406483375*^9}, 3.515962454795875*^9, {
   3.51596250659275*^9, 3.51596259628025*^9}, {3.515962632999*^9, 
   3.515962638608375*^9}, 3.51596268703025*^9, 3.515962971733375*^9, 
   3.515963005624*^9}],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJydkEEKwkAMRYO4cO3KXehJxB6hHiC0ULdCFUpXc5QcZY6So4w/FSppcdPA
D8zPn5dhqu7ZPA5E9DqhNe14G4Z2uh9xqKd33/noAp2hsr9EDd1+RlYmFr2G
EHYEozCRRJAhk4OjcCw4ssY4J0Vng3FOxOzj2OY5umB0zcnLn+KWfsOeNUsJ
GVanzQMzlQIxueLG8qc+l05ouQ==
    "], {{0, 24}, {18, 0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSizeRaw->{18, 24},
  PlotRange->{{0, 18}, {0, 24}}]], "Output",
 CellChangeTimes->{{3.515962326764625*^9, 3.515962351170875*^9}, {
   3.515962387327125*^9, 3.515962406483375*^9}, 3.515962454795875*^9, {
   3.51596250659275*^9, 3.51596259628025*^9}, {3.515962632999*^9, 
   3.515962638608375*^9}, 3.51596268703025*^9, 3.515962971733375*^9, 
   3.51596300565525*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  GraphicsBox[
   TagBox[RasterBox[CompressedData["
1:eJydkEEKwkAMRYO4cO3KXehJxB6hHiC0ULdCFUpXc5QcZY6So4w/FSppcdPA
D8zPn5dhqu7ZPA5E9DqhNe14G4Z2uh9xqKd33/noAp2hsr9EDd1+RlYmFr2G
EHYEozCRRJAhk4OjcCw4ssY4J0Vng3FOxOzj2OY5umB0zcnLn+KWfsOeNUsJ
GVanzQMzlQIxueLG8qc+l05ouQ==
     "], {{0, 24}, {18, 0}}, {0, 255},
     ColorFunction->GrayLevel],
    BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
    Selectable->False],
   BaseStyle->"ImageGraphics",
   ImageSizeRaw->{18, 24},
   PlotRange->{{0, 18}, {0, 24}}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.515962693999*^9, 3.51596278965525*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"bin2", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
   "l1", ",", " ", "l2", ",", " ", "l3", ",", " ", "l4", ",", "l5", ",", 
    "l6"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.51596278359275*^9, 3.51596280484275*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJx9kMsNwjAMhi3EgStiAItJLBihDGC1ElyRChLqyaNkFI+SUcqfNIGSA5Zs
y48vsX0c7t1tQ0SPHUzXv07j2E+XLYLz9LwOqXSA7qHzWqI7jKiqCDMlYSSI
xEII7opEmGcnK/3oUfhQn0kNEd5Lgy/AVwDIOq5AlQTYP8AaILaA5Jl/Aa/r
FUBrTRqANS3zATCe5SXJLB8lfxaYWXAkM0tHiW+3xOM5
      "], {{0, 19}, {17, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{17, 19},
    PlotRange->{{0, 17}, {0, 19}}], ",", 
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJy9UcENwjAMtBAPZoCHxSQRjFAGsFoJvkgFCfXlUTJKR/EowU4TSNJ/LblJ
enf2OTkPz+6xA4DXQT9d/7mMYz/d9nq4Tu/7YNBJ86gZNg0KkrciJeCJgNEj
EkqNBC9qU0FlrNwqMC8rzhVgGkorrDSxAS+Mf8zagJPGtRqMjFYSgrPWDrKR
opr9A7JsNRjJYn18BfhcxmTYapYNNwhzmsco1ajyO1PbKN+BIZU7dc2civkS
EZvR6Ty8uhyzwEBSPO5W8QUi5z2v
      "], {{0, 27}, {26, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{26, 27},
    PlotRange->{{0, 26}, {0, 27}}], ",", 
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJytj7ENAjEMRb8QBRtQRkwSwQjHANadBC3SgYSuyigexaN4lOAcNMQuKLAU
R3l2vr8P0224bgDcd5aG8Xmc53E5b+1xWh6XqZX2n1P/HVw8E6SkbpaR0lLH
GckyAdxhUbt6vAalaBEiiDiK3MY6h1XIKl6k1oLVkBcCB1ZMneLuL/H3T8nd
Oty61DUbBmnvj9UW18w5GKeRh5/iBZXm0lc=
      "], {{0, 23}, {23, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{23, 23},
    PlotRange->{{0, 23}, {0, 23}}], ",", 
   GraphicsBox[
    TagBox[
     RasterBox[{{255, 255, 255, 223, 95, 95, 223}, {255, 255, 255, 127, 0, 0, 
      127}, {255, 255, 255, 159, 0, 0, 159}, {255, 255, 255, 255, 159, 191, 
      255}, {255, 255, 255, 255, 255, 255, 255}, {255, 223, 191, 159, 127, 95,
       191}, {255, 191, 63, 0, 0, 0, 191}, {255, 255, 255, 31, 0, 0, 191}, {
      255, 255, 255, 31, 0, 0, 223}, {255, 255, 255, 31, 0, 0, 223}, {255, 
      255, 255, 31, 0, 0, 255}, {255, 255, 255, 0, 0, 0, 255}, {255, 255, 255,
       0, 0, 0, 255}, {255, 255, 223, 0, 0, 31, 255}, {255, 255, 223, 0, 0, 
      31, 255}, {255, 255, 159, 0, 0, 31, 255}, {255, 31, 0, 0, 0, 0, 63}}, {{
      0, 17}, {7, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{7, 17},
    PlotRange->{{0, 7}, {0, 17}}], ",", 
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJzdUcEJAjEQHMSHRQjBSoKWcBYQ7uD8Cqcg90opW8qWsqWsSU4/d7sW4EBC
wjCT2clpuHe3HYDHoWxd/zpPUz9f9+VymZ/jUKnjZ+n/gEVEYzsSg1dkQIwg
1URAXimlFNGEFGhjm4uymuVkvBmWBs0aM6owJbbIJmTLsqQAkgaTasIctym/
whgdnZYZxeMIIXucwotS07BLmX0tEMeyfBuFIE4MwPidBuYfs7Ebw8Ib48TP
aA==
      "], {{0, 28}, {28, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{28, 28},
    PlotRange->{{0, 28}, {0, 28}}], ",", 
   GraphicsBox[
    TagBox[RasterBox[CompressedData["
1:eJydkEEKwkAMRYO4cO3KXehJxB6hHiC0ULdCFUpXc5QcZY6So4w/FSppcdPA
D8zPn5dhqu7ZPA5E9DqhNe14G4Z2uh9xqKd33/noAp2hsr9EDd1+RlYmFr2G
EHYEozCRRJAhk4OjcCw4ssY4J0Vng3FOxOzj2OY5umB0zcnLn+KWfsOeNUsJ
GVanzQMzlQIxueLG8qc+l05ouQ==
      "], {{0, 24}, {18, 0}}, {0, 255},
      ColorFunction->GrayLevel],
     BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
     Selectable->False],
    BaseStyle->"ImageGraphics",
    ImageSizeRaw->{18, 24},
    PlotRange->{{0, 18}, {0, 24}}]}], "}"}]], "Output",
 CellChangeTimes->{{3.515962748514625*^9, 3.51596277628025*^9}, 
   3.515962807514625*^9, 3.515962976670875*^9, 3.51596300953025*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"dims", "=", 
   RowBox[{"Max", "/@", 
    RowBox[{"Transpose", "[", 
     RowBox[{"ImageDimensions", "/@", 
      RowBox[{"Flatten", "[", "bin2", "]"}]}], "]"}]}]}], ";"}], "\n", 
 RowBox[{"final", " ", "=", " ", 
  RowBox[{"ImageAssemble", "[", 
   RowBox[{"bin2", "/.", 
    RowBox[{"x_Image", "\[RuleDelayed]", 
     RowBox[{"ImageCrop", "[", 
      RowBox[{"x", ",", "dims", ",", 
       RowBox[{"Padding", "\[Rule]", "Automatic"}]}], "]"}]}]}], "]"}]}]}], \
"Input",
 CellChangeTimes->{{3.515962121249*^9, 3.515962165452125*^9}, {
  3.515962811233375*^9, 3.515962830670875*^9}}],

Cell[BoxData[
 GraphicsBox[
  TagBox[RasterBox[CompressedData["
1:eJztlkGO1TAMQCPEgkMgWZwkgiMMB7BmpGGLNCChWfko3nILH8VHKXbahqRx
SgfNwGYs9f/f79p+sZ3UH+6+3nx5k1L69s4+bm5/fHx4uH38/NZuPj1+v79z
1Xu7ftq1vICkE3mJeH8rr5zPK6+cnahId4+LVpUuF6SjUkR9MieyBwqCaUbE
nAGKM2hVjJgIGABBZ5xMU05KiZ7KKQwJkHPAmVImZhZBc8Y9RMkHgfGGYSQB
aKvqqNjcjZziS145WJIMPp0iwLRYtAFbRvGoNSNZv2F0aYjkH6l9vKFiWUbO
RSDl7AlBTnvsRiKK4mx34AvRkaRYaVg4Lm2CTRUGqqjuumeMoRr2ISXgFNq+
h6pvcQo7RavkJFqeiThVmFDC/iTLqKPQ6LJ0y5CuViBqC+9ZX0dNwSgIcd0l
e2LifbSZhD2P00i7OlrGdgLIpGnMDlNzmHVUMOW06lhCEWUZBaKmreIOI3X2
DeSZCXvGwHJnd+ScnZ8loYKhx1mo3TBP8C2VfoVOxTsjT/b7CSd7fSAk4bP2
pFnzQlmdarjJqm2NeOQM9/vml3Lo8aw/dVL1svlks+7fVY3YErj+bKjSCac5
zjHOWX/mKUTdzDR9JDd77DJnPfAGOelP+q3T/hmi3R0MkVrPEaf6yqac9o6g
yJlOF1BfOgUsD6r1F44Nut77aVA11zmXSXm4tucxXFd1wF5XV4fjCcqFpk1n
x8nnnMG8sP6/9accDepeKTTU6excItqs+cDJbmgTZnMstZxlTsxTzvCdubng
FSt3//uYZDBlCD1ue80lFvoc2Vu5I7V3puZuVkwn0kcNy65K9u5PsM6T3IcD
gGyzMhH5EKqHZZMxomI0YRfP/e0lThs7LWjkUNWKZhckv+KIzyLX8pk8Zad+
9AUZN4I/c4qcT27/QK7lU/4b3y7XOJ8mvwAM584u
    "], {{0, 28}, {168, 0}}, {0, 255},
    ColorFunction->GrayLevel],
   BoxForm`ImageTag["Byte", ColorSpace -> "Grayscale", Interleaving -> None],
   Selectable->False],
  BaseStyle->"ImageGraphics",
  ImageSizeRaw->{168, 28},
  PlotRange->{{0, 168}, {0, 28}}]], "Output",
 CellChangeTimes->{{3.5159621444365*^9, 3.515962169514625*^9}, 
   3.515962818702125*^9, 3.51596298103025*^9, 3.515963012749*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"TextRecognize", "[", "final", "]"}]}]], "Input",
 CellChangeTimes->{{3.515962173420875*^9, 3.515962187920875*^9}, {
  3.515962834358375*^9, 3.515962838733375*^9}}],

Cell[BoxData["\<\"ZB3|]yL\"\>"], "Output",
 CellChangeTimes->{
  3.515962191499*^9, 3.515962843108375*^9, {3.515962986170875*^9, 
   3.51596301609275*^9}}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.515962101624*^9, 3.51596210165525*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.515961571514625*^9, 3.51596157334275*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.5159582680615*^9, 3.515958289202125*^9}}]
},
WindowSize->{1098, 679},
WindowMargins->{{4, Automatic}, {Automatic, -46}},
FrontEndVersion->"8.0 for Microsoft Windows (32-bit) (February 23, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 66, 1, 31, "Input"],
Cell[CellGroupData[{
Cell[648, 25, 4189, 101, 140, "Input"],
Cell[4840, 128, 1470, 31, 36, "Output"],
Cell[6313, 161, 6280, 117, 40, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12630, 283, 2003, 48, 152, "Input"],
Cell[14636, 333, 866, 18, 34, "Output"],
Cell[15505, 353, 910, 19, 42, "Output"],
Cell[16418, 374, 858, 18, 38, "Output"],
Cell[17279, 394, 1204, 21, 32, "Output"],
Cell[18486, 417, 884, 19, 43, "Output"],
Cell[19373, 438, 849, 18, 39, "Output"]
}, Open  ]],
Cell[20237, 459, 653, 15, 62, InheritFromParent],
Cell[CellGroupData[{
Cell[20915, 478, 243, 6, 31, InheritFromParent],
Cell[21161, 486, 3971, 88, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[25169, 579, 619, 16, 52, InheritFromParent],
Cell[25791, 597, 1425, 27, 43, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[27253, 629, 221, 4, 52, "Input"],
Cell[27477, 635, 155, 3, 30, "Output"]
}, Open  ]],
Cell[27647, 641, 88, 1, 31, InheritFromParent],
Cell[27738, 644, 91, 1, 31, InheritFromParent],
Cell[27832, 647, 90, 1, 31, InheritFromParent]
}
]
*)

(* End of internal cache information *)
