%FiltroLaplace.m
% Aplicando filtro Laplace na figura A

function Laplace = FiltroLaplace (A)	
	mascara = [	
			-1 -1 -1; 
			-1  8 -1; 
			-1 -1 -1
		 ];

	K = round(size(mascara,1)/2)-1;
	Laplace = uint8(MatrizZeros(size(A,1), size(A,2)));
	A = replicamatrizx(A, K);

	for i=1+K:size(A,1)-K
      for j=1+K:size(A,2)-K
      	valor = abs(
     					sum(
     							sum(double(A(i-K:i+K,j-K:j+K)).* mascara)
     						)
     				);
      	if valor < 0
      		valor = 0;
      	else if valor > 255
      		valor = 255;
        end
      	Laplace(i-K,j-K) = valor;
      end
	end
end