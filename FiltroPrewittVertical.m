%FiltroPrewittVertical.m
% Realizando o filtro prewitt vertical na figura A

function PrewittV = FiltroPrewittVertical (A)
	hv = [
			1  0  -1;
      1  0  -1;
      1  0  -1
		 ]; 

	K = round(size(hv,1)/2)-1;
	PrewittV = uint8(MatrizZeros(size(A,1), size(A,2)));
	A = replicamatrizx(A, K);

	for i=1+K:size(A,1)-K
    for j=1+K:size(A,2)-K
    	valor = abs(
   					sum(
   							sum(double(A(i-K:i+K,j-K:j+K)).* hv)
   						)
   				);
    	if valor < 0
    		valor = 0;
    	else if valor > 255
    		valor = 255;
    	end
      PrewittV(i-K,j-K) = valor;
    end
	end
end