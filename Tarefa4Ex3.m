%Tarefa4Ex3.m

function Tarefa4Ex3 (A, map)
    % ----------- ex 3
    MComum = 1/9*ones(3,3);
    % P = 0
    P0 = imfilter(A, MComum, 0);
    % figure(2);
    % imshow(P0, map)
    imwrite(P0, map, 'Lista4Parte1/ex3/media_p0.png', 'png')

    % P = 255
    P255 = imfilter(A, MComum, 255);
    % figure(3);
    % imshow(P255, map)
    imwrite(P255, map, 'Lista4Parte1/ex3/media_p255.png', 'png')

    % simetria
    simetria = imfilter(A, MComum, 'symmetric');
    % figure(4);
    % imshow(simetria, map)
    imwrite(simetria, map, 'Lista4Parte1/ex3/media_simetria.png', 'png')

    % replica
    replica = imfilter(A, MComum, 'replicate');
    % figure(5);
    % imshow(replica, map)
    imwrite(replica, map, 'Lista4Parte1/ex3/media_replica.png', 'png')

    % circular
    circular = imfilter(A, MComum, 'circular');
    % figure(6);
    % imshow(circular, map)
    imwrite(circular, map, 'Lista4Parte1/ex3/media_circular.png', 'png')
end