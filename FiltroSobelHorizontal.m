%FiltroSobelHorizontal.m
% Aplicando filtro Sobel horizontal na figura A

function SobelF = FiltroSobelHorizontal (A)	
	hh = [	
			-1 -2 -1; 
			 0  0  0; 
			 1  2  1
		 ];

	K = round(size(hh,1)/2)-1;
	SobelF = uint8(MatrizZeros(size(A,1), size(A,2)));
	A = replicamatrizx(A, K);

	for i=1+K:size(A,1)-K
      for j=1+K:size(A,2)-K
      	valor = abs(
     					sum(
     							sum(double(A(i-K:i+K,j-K:j+K)).* hh)
     						)
     				);
      	if valor < 0
      		valor = 0;
      	else if valor > 255
      		valor = 255;
        end
      	SobelF(i-K,j-K) = valor;
      end
	end
end