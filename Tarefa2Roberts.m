%Tarefa2Roberts.m
% Parte da tarefa 2 relativo aos testes com Roberts

function void = Tarefa2Roberts (F, M)
	RobertsFH = FiltroRobertsHorizontal (F);
	RobertsFV = FiltroRobertsVertical (F);

	figure(10);
	imshow (RobertsFH, M)

	figure(11);
	imshow (RobertsFV, M)

	% And 
	%figure(12);
	%imshow (bitand(RobertsFH, RobertsFV), M)
	imwrite(bitand(RobertsFH, RobertsFV),'roberts_and.png','png')
	% Or
	%figure(13);
	%imshow (bitor(RobertsFH, RobertsFV), M)
	imwrite(bitor(RobertsFH, RobertsFV),'roberts_or.png','png')
	% Plus
	%figure(14);
	%imshow (PlusAB(RobertsFH, RobertsFV), M)
	imwrite(PlusAB(RobertsFH, RobertsFV),'roberts_plus.png','png')
	% Xor
	%figure(15);
	%imshow (bitxor(RobertsFH, RobertsFV), M) 
	imwrite(bitxor(RobertsFH, RobertsFV),'roberts_xor.png','png')
end