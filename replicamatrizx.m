%replicamatrizx.m

function A2 = replicamatrizx(A,K)
	for i=1:K
		A2 = [
				A(1,1)		A(1,:)		A(1,end);
				A(:,1)		A		A(:,end);
				A(end,1)	A(end,:) 	A(end,end)
			 ];	
		A = A2;	
	end
end
