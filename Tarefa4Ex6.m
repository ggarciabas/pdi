%Tarefa4Ex6.m

function Tarefa4Ex6 (A, mapA, B, mapB)
    % Mediana 5x5
    Mediana5x5A = filtromediana (A, 5);
    Mediana5x5B = filtromediana (B, 5);    
    Mediana5x5PlusMediana5x5 = PlusAB(Mediana5x5A, Mediana5x5B);
    imwrite(Mediana5x5PlusMediana5x5, mapA, 'Lista4Parte1/ex6/mediana/+/mediana_5x5_A+B.png', 'png')
    Mediana5x5MinusMediana5x5 = MinusAB(Mediana5x5A, Mediana5x5B);
    imwrite(Mediana5x5MinusMediana5x5, mapA, 'Lista4Parte1/ex6/mediana/-/mediana_5x5_A-B.png', 'png')
    
    % Soma depois faz mediana 5x5
    AB = PlusAB(A, B);
    Mediana5x5 = filtromediana (AB, 5);
    imwrite(Mediana5x5, mapA, 'Lista4Parte1/ex6/mediana/+/A+B_mediana_5x5.png', 'png')
    
    % Subtrai depois faz mediana 5x5
    AB = MinusAB(A, B);
    Mediana5x5 = filtromediana (AB, 5);
    imwrite(Mediana5x5, mapA, 'Lista4Parte1/ex6/mediana/-/A-B_mediana_5x5.png', 'png')
    
    % Media 5x5
    Media5x5A = filtromedia (A, 5);
    Media5x5B = filtromedia (B, 5);    
    Media5x5PlusMedia5x5 = PlusAB(Media5x5A, Media5x5B);
    imwrite(Media5x5PlusMedia5x5, mapA, 'Lista4Parte1/ex6/media/+/media_5x5_A+B.png', 'png')
    Media5x5MinusMedia5x5 = MinusAB(Media5x5A, Media5x5B);
    imwrite(Media5x5MinusMedia5x5, mapA, 'Lista4Parte1/ex6/media/-/media_5x5_A-B.png', 'png')
    
    % Soma depois faz media 5x5
    AB = PlusAB(A, B);
    Media5x5 = filtromedia (AB, 5);
    imwrite(Media5x5, mapA, 'Lista4Parte1/ex6/media/+/A+B_media_5x5.png', 'png')
    
    % Subtrai depois faz mediana 5x5
    AB = MinusAB(A, B);
    Media5x5 = filtromedia (AB, 5);
    imwrite(Media5x5, mapA, 'Lista4Parte1/ex6/media/-/A-B_media_5x5.png', 'png')
    
end