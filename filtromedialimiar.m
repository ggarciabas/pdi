%filtromedialimiar.m

% Filtro Media 
% A = matriz da imagem
% N = parametro do filtro da media (3, 5, 7, 9, ...)
% M = valor limiar para troca dos valores

% Verificar funcao media para funcionamento.

function A3 = filtromedialimiar (A, N, M)
   K = round(N/2)-1;
   A2 = replicamatrizx(A,K);
   for i=K+1:size(A2,1)-K
       for j=K+1:size(A2,2)-K
          resp = sum(sum(A2(i-K:i+K,j-K:j+K)))/(N^2);
          if (abs(resp-A(i-K,j-K))) <= M
              A3(i-K,j-K) = resp;
          else
              A3(i-K,j-K) = A(i-K,j-K);
          end
       end
   end
   A3 = round(A3);
end
