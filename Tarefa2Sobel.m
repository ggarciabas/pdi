%Tarefa2Sobel.m
% Parte da tarefa 2 relativo aos testes com Sobel

function void = Tarefa2Sobel (F, M)
	SobelFH = FiltroSobelHorizontal (F);
	SobelFV = FiltroSobelVertical (F);

	%figure(3);
	%imshow (SobelFH, M)

	%figure(4);
	%imshow (SobelFV, M)

	% And 
	%figure(5);
	%imshow (bitand(SobelFH, SobelFV), M)
	imwrite(bitand(SobelFH, SobelFV),'sobel_and.png','png')
	% Or
	%figure(6);
	%imshow (bitor(SobelFH, SobelFV), M)
	imwrite(bitor(SobelFH, SobelFV),'sobel_or.png','png')
	% Plus
	%figure(7);
	%imshow (PlusAB(SobelFH, SobelFV), M)
	imwrite(PlusAB(SobelFH, SobelFV),'sobel_plus.png','png')
	% Xor
	%figure(8);
	%imshow (bitxor(SobelFH, SobelFV), M) 
	imwrite(bitxor(SobelFH, SobelFV),'sobel_xor.png','png')
end