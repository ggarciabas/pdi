%Tarefa4Ex5.m

function Tarefa4Ex5 (A, map)
    % ----------- ex 5
    
    % Mediana 3x3
    Mediana3x3 = filtromediana (A, 3);
    imwrite(Mediana3x3, map, 'Lista4Parte1/ex5/mediana_3x3.png', 'png')
    
    % Mediana 5x5
    Mediana5x5 = filtromediana (A, 5);
    imwrite(Mediana5x5, map, 'Lista4Parte1/ex5/mediana_5x5.png', 'png')
    
    % Mediana 7x7
    Mediana7x7 = filtromediana (A, 7);
    imwrite(Mediana7x7, map, 'Lista4Parte1/ex5/mediana_7x7.png', 'png')
    
    % Mediana 9x9
    Mediana9x9 = filtromediana (A, 9);
    imwrite(Mediana9x9, map, 'Lista4Parte1/ex5/mediana_9x9.png', 'png')
    
end