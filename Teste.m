%Teste.m
close all;
clear all;
clc;

[A, map] = imread ("captcha2.jpg");
A = A(:,:,1);

% Media 5x5
N=5;
Media5x5 = filtromedia (A, N);
figure(1);
imshow(Media5x5, map)

A = Media5x5;

% Media 9x9 limiar 30
N = 9;
M = 30;
Media9x9L30 = filtromedialimiar (A, N, M);
figure(2);
imshow(Media9x9L30, map)

A = Media9x9L30;

% Media 9x9 limiar 10
N = 9;
M = 10;
Media9x9L10 = filtromedialimiar (A, N, M);
figure(3);
imshow(Media9x9L10, map)

A = Media9x9L10;






