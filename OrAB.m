%OrAB.m
% Realizado operacao aritmetica OR nas matrizes A e B

function Or = OrAB (A, B)
	Or = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			if A(i,j) == 1 || B(i,j) == 1
				Or(i,j) = 1;
			end
		end
	end 
end