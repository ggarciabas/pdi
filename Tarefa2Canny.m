%Tarefa2Canny.m

function void = Tarefa2Canny (F, M)
	CannyV = FiltroCannyVertical (F);

	%figure(9);
	%imshow (Canny, M) 
	imwrite(CannyV,'canny_vertical.png','png')

	CannyH = FiltroCannyHorizontal (F);
	imwrite(CannyH,'canny_horizontal.png','png')

	CannyD1 = FiltroCannyDiagonal1 (F);
	imwrite(CannyD1,'canny_diagonal1.png','png')

	CannyD2 = FiltroCannyDiagonal2 (F);
	imwrite(CannyD2,'canny_diagonal2.png','png')
end