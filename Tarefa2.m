%Tarefa2.m

close all; clear all; clc;

[F1, M1] = imread ('ilha.bmp');

%figure(1);
%imshow (F1, M1)

%figure(2);
%imshow (F2, M2)

% Aplicando filtro Sobel	
	%Tarefa2Sobel (F1, M1);
	
% Aplicando filtro Laplace
	%Tarefa2Laplace (F1, M1);

% Aplicando filtro Roberts
	%Tarefa2Roberts (F1, M1);

% Aplicando filtro Prewitt
	%Tarefa2Prewitt (F1, M1);

% Aplicando filtro Canny
	%Tarefa2Canny (F1, M1);