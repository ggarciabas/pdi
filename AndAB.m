%AndAB.m
% Realizando a operacao aritmetica AND com as matrizes A e B

function And = AndAB (A, B)
	And = uint8(MatrizZeros(size(A,1), size(A,2)));
	for i=1:size(A,1)
		for j=1:size(A,2)
			if A(i,j) == B (i,j)
				if A(i,j) == 1
					And(i,j) = 1;
				end
			end
		end
	end 
end